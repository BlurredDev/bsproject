const compression = require('compression')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')


module.exports = (app, express) => {
    app.use(express.static('dist'))
        .set('views','dist/views')
        .engine('html', require('ejs').renderFile)
        .set('view engine', 'html')
        .use(compression())

        .use((err, req, res, next) => {
            console.error(err.stack)
            res.status(400).send(err.message)
        })

        .use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*')
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
            next()
        })
}