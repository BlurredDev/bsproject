// Requis
const gulp = require('gulp');

// Include plugins
const $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    replaceString: /\bgulp[\-.]/
});
const gutil = require('gulp-util');
// Variables de chemins
const source = './src';
const destination = './dist';

//Tache Css
gulp.task('bowerLess', () => {
    return gulp.src($.mainBowerFiles())
        .pipe($.filter('**/*.less'))
        .pipe($.less())
        .pipe($.cssbeautify({ indent: '  ' }))
        .pipe($.autoprefixer())
        .pipe(gulp.dest(source + '/assets/styles'));
});
gulp.task('appLess', () => {
    return gulp.src(source + '/assets/app_components/less/*.*less')
        .pipe($.less())
        .pipe($.csscomb())
        .pipe($.cssbeautify({ indent: '  ' }))
        .on('error', (err) => { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe($.autoprefixer())
        .pipe(gulp.dest(source + '/assets/styles'));
});
gulp.task('css', () => {
    return gulp.src(source + '/assets/styles/**.css')
        .on('error', (err) => { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        /*.pipe($.uncss({
            html: [source + '/{,views/}/{,_includes/}*.html']
        }))
        //A utiliser pour nettoyer le css lors de la mise en prod
        //A Améliorer de manière à ne pas supprimer les classes dynamiques si il y en a
        */
        .pipe($.cleanCss({ debug: true }, function (details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe($.rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(destination + '/assets/css/'));
});
// Tâches Javascript
//Extraction des script des composant bower
gulp.task('bowerJs', () => {
    return gulp.src($.mainBowerFiles())
        .pipe($.filter('**/*.js'))
        .pipe(gulp.dest(source + '/assets/js'));
})
//Extraciton des script propre à l'application
gulp.task('appJs', () => {
    return gulp.src(source + '/assets/app_components/js/*.js')
        .pipe($.concat('global.js'))
        .pipe(gulp.dest(source + '/assets/js'));
});
//Minification et envoi dans le fichier de destination
gulp.task('js', () => {
    return gulp.src(source + '/assets/js/*.js')
        .pipe($.babel({
            presets: ['es2015'],
            compact: true
        }))
        .pipe($.uglify())
        .pipe($.rename({
            suffix: '.min'
        }))
        .on('error', (err) => { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(destination + '/assets/js/'));
});
// Tâche "img" = Images optimisées
gulp.task('img', () => {
    return gulp.src(source + '/assets/img/*.{png,jpg,jpeg,gif,svg}')
        .pipe($.imagemin())
        .pipe(gulp.dest(destination + '/assets/img'));
});
// Tâche "html" = includes HTML
gulp.task('html', () => {
    return gulp.src(source + '/{,views/}/{,_includes/}*.html')
        // Generates HTML includes
        .pipe($.htmlExtend({
            annotations: false,
            verbose: false
        })) // default options
        .pipe(gulp.dest(destination))
});


// Tâche "watch" = je surveille *less
gulp.task('watch', () => {
    gulp.watch(source + '/views/*.html', gulp.series('html'));
    gulp.watch(source + '/assets/app_components/js/*.js', gulp.series('appJs', 'js'));
    gulp.watch(source + '/assets/app_components/less/*.less', gulp.series('appLess', 'css', 'img'));
});
gulp.task('deploy', () => {
    return $.git.push('heroku', 'master', function (err) {
        if (err) {
            console.log(err); throw err;
        }
    })
})
//Tâche "fastbuild"
gulp.task('fastbuild',gulp.series('appLess','css','img','html'))
// Tâche "build"
gulp.task('build', gulp.series('bowerJs', 'appJs', 'bowerLess', 'appLess', 'js', 'img', 'css', 'html'));
// Tâche par défaut
gulp.task('default', gulp.series('build'));