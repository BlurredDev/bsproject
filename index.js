const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const CONCURRENCY = process.env.WEB_CONCURRENCY || 1;


if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    const express = require('express')
    const app = express();
    //MiddleWares
    const middlewares = require('./lib/middlewares.js')
    //Route file
    const mainRoutes = require('./lib/routes/mainRoutes.js')

    middlewares(app, express)
    mainRoutes(app)
    const port = process.env.PORT || 3000
    app.listen(port,()=>{
        console.log('Le serveur ecoute sur le port : ' , port)
    })
    console.log(`Worker ${process.pid} started`);
}